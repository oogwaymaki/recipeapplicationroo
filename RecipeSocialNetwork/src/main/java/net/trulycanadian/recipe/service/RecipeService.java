package net.trulycanadian.recipe.service;

import net.trulycanadian.recipe.model.Recipe;

import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { net.trulycanadian.recipe.model.Recipe.class })
public interface RecipeService {
}
