package net.trulycanadian.recipe.api;

import java.util.UUID;

import net.trulycanadian.recipe.model.Recipe;
import net.trulycanadian.recipe.model.SimpleRecipe;
import net.trulycanadian.recipe.model.Useraccount;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api/recipe")
@RooWebJson(jsonObject = Recipe.class)
public class RecipeAPIConroller {
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<String> createFromJson(@RequestBody String json) {
		SimpleRecipe simpleRecipe = SimpleRecipe.fromJsonToSimpleRecipe(json);

		Recipe recipe = new Recipe();
		recipe.setName(simpleRecipe.getName());
		recipe.setCost(simpleRecipe.getCost());
		recipe.setHealthrating(simpleRecipe.getHealthrating());
		recipe.setTasteRating(simpleRecipe.getTasteRating());
		recipe.setDirections(simpleRecipe.getDirections());
		String uuid = UUID.randomUUID().toString();
		recipe.setUuid(uuid);
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String name = auth.getName();
		Useraccount userAccount = Useraccount.getUserAccount(name);
		recipe.setUseraccount(userAccount);
		recipeService.saveRecipe(recipe);
		String jsonUuid = "{\"uuid\": \"" + uuid + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		
		return new ResponseEntity<String>(jsonUuid, headers, HttpStatus.CREATED);

	}

}
