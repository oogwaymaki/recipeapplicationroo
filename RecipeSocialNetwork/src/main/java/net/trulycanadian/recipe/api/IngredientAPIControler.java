package net.trulycanadian.recipe.api;

import net.trulycanadian.recipe.model.Ingredient;
import net.trulycanadian.recipe.model.Recipe;
import net.trulycanadian.recipe.model.SimpleIngredient;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.json.RooWebJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api/ingredients")
@RooWebJson(jsonObject = Ingredient.class)
public class IngredientAPIControler {
	@RequestMapping(value = "/jsonArray", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<String> createFromJsonArray(@RequestBody String json) {
		
		for (SimpleIngredient ingredient : SimpleIngredient.fromJsonArrayToSimpleIngredients(json)) {
			
			Recipe recipe = Recipe.getRecipeByUUID(ingredient.getUuid());
			Ingredient realIngredient = new Ingredient();
			realIngredient.setName(ingredient.getName());
			realIngredient.setMeasurement(ingredient.getMeasurement());
			realIngredient.setRecipe(recipe);
			realIngredient.setUuid(ingredient.getUuid());
			realIngredient.setUnit(ingredient.getUnit());
			realIngredient.persist();
		}
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

}
