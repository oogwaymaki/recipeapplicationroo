// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.controller;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import net.trulycanadian.recipe.controller.DurationController;
import net.trulycanadian.recipe.model.Duration;
import net.trulycanadian.recipe.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect DurationController_Roo_Controller {
    
    @Autowired
    RecipeService DurationController.recipeService;
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String DurationController.create(@Valid Duration duration, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, duration);
            return "databaserecord/durations/create";
        }
        uiModel.asMap().clear();
        duration.persist();
        return "redirect:/databaserecord/durations/" + encodeUrlPathSegment(duration.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String DurationController.createForm(Model uiModel) {
        populateEditForm(uiModel, new Duration());
        return "databaserecord/durations/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String DurationController.show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("duration", Duration.findDuration(id));
        uiModel.addAttribute("itemId", id);
        return "databaserecord/durations/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String DurationController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("durations", Duration.findDurationEntries(firstResult, sizeNo));
            float nrOfPages = (float) Duration.countDurations() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("durations", Duration.findAllDurations());
        }
        return "databaserecord/durations/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String DurationController.update(@Valid Duration duration, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, duration);
            return "databaserecord/durations/update";
        }
        uiModel.asMap().clear();
        duration.merge();
        return "redirect:/databaserecord/durations/" + encodeUrlPathSegment(duration.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String DurationController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Duration.findDuration(id));
        return "databaserecord/durations/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String DurationController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Duration duration = Duration.findDuration(id);
        duration.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/databaserecord/durations";
    }
    
    void DurationController.populateEditForm(Model uiModel, Duration duration) {
        uiModel.addAttribute("duration", duration);
        uiModel.addAttribute("recipes", recipeService.findAllRecipes());
    }
    
    String DurationController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
