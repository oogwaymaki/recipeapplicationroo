package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Cuisine;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/cuisines")
@Controller
@RooWebScaffold(path = "databaserecord/cuisines", formBackingObject = Cuisine.class)
public class CuisineController {
}
