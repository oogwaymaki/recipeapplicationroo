package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Link;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/links")
@Controller
@RooWebScaffold(path = "databaserecord/links", formBackingObject = Link.class)
public class LinkController {
}
