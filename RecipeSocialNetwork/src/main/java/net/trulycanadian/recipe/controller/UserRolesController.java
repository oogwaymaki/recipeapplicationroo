package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.UserRoles;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/userroleses")
@Controller
@RooWebScaffold(path = "databaserecord/userroleses", formBackingObject = UserRoles.class)
public class UserRolesController {
}
