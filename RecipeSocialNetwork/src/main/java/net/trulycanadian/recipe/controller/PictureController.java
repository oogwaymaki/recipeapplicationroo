package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Picture;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/pictures")
@Controller
@RooWebScaffold(path = "databaserecord/pictures", formBackingObject = Picture.class)
public class PictureController {
}
