package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.CuisineType;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/cuisinetypes")
@Controller
@RooWebScaffold(path = "databaserecord/cuisinetypes", formBackingObject = CuisineType.class)
public class CuisineTypeController {
}
