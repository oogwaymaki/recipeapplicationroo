package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.NutriFacts;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/nutrifactses")
@Controller
@RooWebScaffold(path = "databaserecord/nutrifactses", formBackingObject = NutriFacts.class)
public class NutriFactsController {
}
