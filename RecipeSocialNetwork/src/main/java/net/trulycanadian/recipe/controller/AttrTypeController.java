package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.AttrType;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/attrtypes")
@Controller
@RooWebScaffold(path = "databaserecord/attrtypes", formBackingObject = AttrType.class)
public class AttrTypeController {
}
