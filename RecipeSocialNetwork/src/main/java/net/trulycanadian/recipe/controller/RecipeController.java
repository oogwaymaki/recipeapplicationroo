package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Recipe;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/recipes")
@Controller
@RooWebScaffold(path = "databaserecord/recipes", formBackingObject = Recipe.class)
public class RecipeController {
}
