package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Duration;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/durations")
@Controller
@RooWebScaffold(path = "databaserecord/durations", formBackingObject = Duration.class)
public class DurationController {
}
