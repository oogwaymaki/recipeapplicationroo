package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Useraccount;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/useraccounts")
@Controller
@RooWebScaffold(path = "databaserecord/useraccounts", formBackingObject = Useraccount.class)
public class UseraccountController {
}
