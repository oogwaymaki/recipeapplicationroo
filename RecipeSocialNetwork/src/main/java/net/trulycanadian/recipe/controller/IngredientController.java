package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Ingredient;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/ingredients")
@Controller
@RooWebScaffold(path = "databaserecord/ingredients", formBackingObject = Ingredient.class)
public class IngredientController {
}
