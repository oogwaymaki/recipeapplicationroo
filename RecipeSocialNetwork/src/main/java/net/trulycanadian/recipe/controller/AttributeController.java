package net.trulycanadian.recipe.controller;

import net.trulycanadian.recipe.model.Attribute;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/databaserecord/attributes")
@Controller
@RooWebScaffold(path = "databaserecord/attributes", formBackingObject = Attribute.class)
public class AttributeController {
}
