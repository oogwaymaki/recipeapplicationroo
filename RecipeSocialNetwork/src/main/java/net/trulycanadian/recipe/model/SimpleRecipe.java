package net.trulycanadian.recipe.model;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.json.RooJson;


@RooJavaBean
@RooJson

public class SimpleRecipe {
	private String name;
	private int healthrating;
	private int tasteRating;
	private int servings;
	private double cost;
	private String directions;


}
