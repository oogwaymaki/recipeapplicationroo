// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.trulycanadian.recipe.model.Comment;

privileged aspect Comment_Roo_Json {
    
    public String Comment.toJson() {
        return new JSONSerializer().exclude("*.class").serialize(this);
    }
    
    public static Comment Comment.fromJsonToComment(String json) {
        return new JSONDeserializer<Comment>().use(null, Comment.class).deserialize(json);
    }
    
    public static String Comment.toJsonArray(Collection<Comment> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
    
    public static Collection<Comment> Comment.fromJsonArrayToComments(String json) {
        return new JSONDeserializer<List<Comment>>().use(null, ArrayList.class).use("values", Comment.class).deserialize(json);
    }
    
}
