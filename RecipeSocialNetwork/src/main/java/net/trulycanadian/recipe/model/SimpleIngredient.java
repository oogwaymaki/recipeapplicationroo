package net.trulycanadian.recipe.model;

import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooJson
public class SimpleIngredient {
	private String name;
	private double measurement;
	private String uuid;
	private String unit;
}
