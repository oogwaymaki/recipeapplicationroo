// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import net.trulycanadian.recipe.model.Cuisine;

privileged aspect Cuisine_Roo_Jpa_Entity {
    
    declare @type: Cuisine: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Cuisine.id;
    
    @Version
    @Column(name = "version")
    private Integer Cuisine.version;
    
    public Long Cuisine.getId() {
        return this.id;
    }
    
    public void Cuisine.setId(Long id) {
        this.id = id;
    }
    
    public Integer Cuisine.getVersion() {
        return this.version;
    }
    
    public void Cuisine.setVersion(Integer version) {
        this.version = version;
    }
    
}
