package net.trulycanadian.recipe.model;

import java.util.Collection;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

import flexjson.JSONSerializer;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class Ingredient {

    @NotNull
    private String name;

    @NotNull
    private double measurement;

    @ManyToOne
    private Recipe recipe;
    
    private String uuid;
    
    private String unit;
    
    public String toJson() {
        return new JSONSerializer().exclude("uuid").exclude("*.class").serialize(this);
    }
    
    public static String toJsonArray(Collection<Ingredient> collection) {
        return new JSONSerializer().exclude("uuid").exclude("*.class").serialize(collection);
    }
}
