// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.trulycanadian.recipe.model.SimpleIngredient;

privileged aspect SimpleIngredient_Roo_Json {
    
    public String SimpleIngredient.toJson() {
        return new JSONSerializer().exclude("*.class").serialize(this);
    }
    
    public static SimpleIngredient SimpleIngredient.fromJsonToSimpleIngredient(String json) {
        return new JSONDeserializer<SimpleIngredient>().use(null, SimpleIngredient.class).deserialize(json);
    }
    
    public static String SimpleIngredient.toJsonArray(Collection<SimpleIngredient> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
    
    public static Collection<SimpleIngredient> SimpleIngredient.fromJsonArrayToSimpleIngredients(String json) {
        return new JSONDeserializer<List<SimpleIngredient>>().use(null, ArrayList.class).use("values", SimpleIngredient.class).deserialize(json);
    }
    
}
