package net.trulycanadian.recipe.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class Useraccount {

    @NotNull
    private String userName;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private int rating;

    private int accountType;

    private String token;

    private String authType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userid")
    private Set<UserRoles> userroles = new HashSet<UserRoles>();

    private Boolean enabled;

    private String email;

    private String vocation;
    private String uuid;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "useraccount")
    private Set<Recipe> recipes = new HashSet<Recipe>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "useraccount")
    private Set<Comment> comments = new HashSet<Comment>();
    public String toJson() {
        return new JSONSerializer().exclude("uuid").exclude("token").exclude("password").exclude("*.class").serialize(this);
    }
    
    public static Useraccount fromJsonToUseraccount(String json) {
        return new JSONDeserializer<Useraccount>().use(null, Useraccount.class).deserialize(json);
    }
    
    public static String toJsonArray(Collection<Useraccount> collection) {
        return new JSONSerializer().exclude("token").exclude("password").exclude("uuid").exclude("accountType").exclude("comments").exclude("email)").exclude("firstName").exclude("lastName").exclude("recipes").exclude("userroles").exclude("*.class").serialize(collection);
    }
    
    public static Collection<Useraccount> fromJsonArrayToUseraccounts(String json) {
        return new JSONDeserializer<List<Useraccount>>().use(null, ArrayList.class).use("values", Useraccount.class).deserialize(json);
    }

    public static Useraccount getUserAccount(String name) {
        return entityManager().createQuery("SELECT o FROM Useraccount o where user_name = '" + name + "'", Useraccount.class).getSingleResult();
    }
}
