package net.trulycanadian.recipe.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class Recipe {

	@NotNull
	private String name;

	private double cost;
	
	private int healthrating;

	private int tasteRating;

	private int servings;
	@Column(name = "directions", columnDefinition="TEXT")
	private String directions;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
	private Set<Ingredient> ingredients = new HashSet<Ingredient>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
	private Set<Link> links = new HashSet<Link>();



	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
	private Set<Picture> pictures = new HashSet<Picture>();

	@ManyToOne
	private Useraccount useraccount;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
	private Set<Comment> comments = new HashSet<Comment>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
	private Set<Attribute> attributes = new HashSet<Attribute>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
	private Set<Cuisine> cusines = new HashSet<Cuisine>();

	  public static Recipe fromJsonToRecipe(String json) {
	        return new JSONDeserializer<Recipe>().use(null, Recipe.class).deserialize(json);
	    }
	    
	    public static String toJsonArray(Collection<Recipe> collection) {
	        return new JSONSerializer().exclude("useraccount.email").exclude("*.version").exclude("useraccount.version").exclude("useraccount.vocation").exclude("useraccount.enabled").exclude("useraccount.authType").exclude("useraccount.token").exclude("useraccount.password").exclude("useraccount.accountType").exclude("useraccount.comments").exclude("useraccount.email)").exclude("useraccount.firstName").exclude("useraccount.lastName").exclude("useraccount.recipes").exclude("useraccount.userroles").exclude("uuid").exclude("*.class").serialize(collection);
	    }
	    
	    public static Collection<Recipe> fromJsonArrayToRecipes(String json) {
	        return new JSONDeserializer<List<Recipe>>().use(null, ArrayList.class).use("values", Recipe.class).deserialize(json);
	    }
	    
	    public String toJson() {
	        return new JSONSerializer().include("ingredients").include("comments").include("attributes").include("cusines").include("links").exclude("uuid").exclude("*.class").serialize(this);

	    }
	
	    public static Recipe getRecipeByUUID(String name) {
	        return entityManager().createQuery("SELECT o FROM Recipe o where uuid = '" + name + "'", Recipe.class).getSingleResult();
	    }
	    
	private String uuid;
	    
}
