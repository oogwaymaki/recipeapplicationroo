// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.trulycanadian.recipe.model.Duration;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Duration_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Duration.entityManager;
    
    public static final EntityManager Duration.entityManager() {
        EntityManager em = new Duration().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Duration.countDurations() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Duration o", Long.class).getSingleResult();
    }
    
    public static List<Duration> Duration.findAllDurations() {
        return entityManager().createQuery("SELECT o FROM Duration o", Duration.class).getResultList();
    }
    
    public static Duration Duration.findDuration(Long id) {
        if (id == null) return null;
        return entityManager().find(Duration.class, id);
    }
    
    public static List<Duration> Duration.findDurationEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Duration o", Duration.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Duration.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Duration.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Duration attached = Duration.findDuration(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Duration.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Duration.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Duration Duration.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Duration merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
