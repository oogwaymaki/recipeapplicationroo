package net.trulycanadian.recipe.model;

import javax.persistence.ManyToOne;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class NutriFacts {

    private int calories;

    private int satFats;

    private int fats;

    private int sodium;

    private int carbohydrates;

    @ManyToOne
    private Recipe recipe;
}
