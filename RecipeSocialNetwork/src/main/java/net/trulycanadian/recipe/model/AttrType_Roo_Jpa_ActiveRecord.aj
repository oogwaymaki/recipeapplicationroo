// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import net.trulycanadian.recipe.model.AttrType;
import org.springframework.transaction.annotation.Transactional;

privileged aspect AttrType_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager AttrType.entityManager;
    
    public static final EntityManager AttrType.entityManager() {
        EntityManager em = new AttrType().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long AttrType.countAttrTypes() {
        return entityManager().createQuery("SELECT COUNT(o) FROM AttrType o", Long.class).getSingleResult();
    }
    
    public static List<AttrType> AttrType.findAllAttrTypes() {
        return entityManager().createQuery("SELECT o FROM AttrType o", AttrType.class).getResultList();
    }
    
    public static AttrType AttrType.findAttrType(Long id) {
        if (id == null) return null;
        return entityManager().find(AttrType.class, id);
    }
    
    public static List<AttrType> AttrType.findAttrTypeEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM AttrType o", AttrType.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void AttrType.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void AttrType.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            AttrType attached = AttrType.findAttrType(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void AttrType.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void AttrType.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public AttrType AttrType.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        AttrType merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
