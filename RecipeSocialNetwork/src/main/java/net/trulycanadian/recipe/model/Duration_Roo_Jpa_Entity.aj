// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import net.trulycanadian.recipe.model.Duration;

privileged aspect Duration_Roo_Jpa_Entity {
    
    declare @type: Duration: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Duration.id;
    
    @Version
    @Column(name = "version")
    private Integer Duration.version;
    
    public Long Duration.getId() {
        return this.id;
    }
    
    public void Duration.setId(Long id) {
        this.id = id;
    }
    
    public Integer Duration.getVersion() {
        return this.version;
    }
    
    public void Duration.setVersion(Integer version) {
        this.version = version;
    }
    
}
