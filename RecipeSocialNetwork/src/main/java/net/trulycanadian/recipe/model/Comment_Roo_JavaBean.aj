// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import net.trulycanadian.recipe.model.Comment;
import net.trulycanadian.recipe.model.Recipe;
import net.trulycanadian.recipe.model.Useraccount;

privileged aspect Comment_Roo_JavaBean {
    
    public String Comment.getComment() {
        return this.comment;
    }
    
    public void Comment.setComment(String comment) {
        this.comment = comment;
    }
    
    public Recipe Comment.getRecipe() {
        return this.recipe;
    }
    
    public void Comment.setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
    
    public Useraccount Comment.getUseraccount() {
        return this.useraccount;
    }
    
    public void Comment.setUseraccount(Useraccount useraccount) {
        this.useraccount = useraccount;
    }
    
}
