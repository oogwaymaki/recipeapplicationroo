package net.trulycanadian.recipe.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
@RooJson
public class CuisineType {

    @NotNull
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuisineType")
    private Set<Cuisine> cuisine = new HashSet<Cuisine>();
}
