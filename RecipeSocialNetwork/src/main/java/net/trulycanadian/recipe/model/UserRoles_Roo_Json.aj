// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package net.trulycanadian.recipe.model;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.trulycanadian.recipe.model.UserRoles;

privileged aspect UserRoles_Roo_Json {
    
    public String UserRoles.toJson() {
        return new JSONSerializer().exclude("*.class").serialize(this);
    }
    
    public static UserRoles UserRoles.fromJsonToUserRoles(String json) {
        return new JSONDeserializer<UserRoles>().use(null, UserRoles.class).deserialize(json);
    }
    
    public static String UserRoles.toJsonArray(Collection<UserRoles> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
    
    public static Collection<UserRoles> UserRoles.fromJsonArrayToUserRoleses(String json) {
        return new JSONDeserializer<List<UserRoles>>().use(null, ArrayList.class).use("values", UserRoles.class).deserialize(json);
    }
    
}
